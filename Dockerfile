FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

RUN echo "deb [signed-by=/usr/share/keyrings/collaboraonline-release-keyring.gpg] https://collaboraoffice.com/repos/CollaboraOnline/CODE-ubuntu2204 /" > /etc/apt/sources.list.d/collabora.list
RUN curl https://www.collaboraoffice.com/downloads/gpg/collaboraonline-release-keyring.gpg --output /usr/share/keyrings/collaboraonline-release-keyring.gpg

# these lines are to check for new package versions
# RUN apt-get update && apt search coolwsd && apt search code-brand && exit 1

# https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-ubuntu2204/Packages
# - renovate: datasource=github-releases depName=CollaboraOnline/online versioning=semver
# renovate: datasource=docker depName=collabora/code versioning=docker
ARG COOLWSD_VERSION=24.04.13.1.1

# the deb repo only has latest anyways so we ignore those for now
# ARG COOLWSD_VERSION=24.04.8.1-1
# ARG CODE_BRAND_VERSION=24.04-21

# collabora expects hunspell dicts (https://help.nextcloud.com/t/spelling-check-code-3-0/25178/12). NOTE: To add more languages, we have to change coolwsd.xml
# manually select the language packs since we don't want debuginfo installed
# list is produced by apt-get install -y collaboraofficebasis-*
RUN apt-get update && \
    apt-get install -y \
        fonts-open-sans gnupg2 \
        hyphen-en-us hyphen-en-gb hyphen-en-gb hyphen-de hyphen-fr \
        hunspell-{af,an,ar,be,bg,bn,bo,bs,br,ca,cs,da,de-{at,ch,de},dz,el,en-{au,ca,gb,us,za},es,eu,fr,fr-classical,gd,gl,gu,gug,he,hi,hr,hu,id,is,it,kk,kmr,ko,lo,lt,lv,ne,nl,no,oc,pl,pt-{br,pt},ro,ru,si,sk,sl,sr,sv,sw,te,th,tr,uk,uz,vi} && \
    apt-get install -y coolwsd coolwsd-deprecated code-brand && \
    apt-get install -y collaboraoffice-dict* && \
    apt-get install -y collaboraofficebasis-{ar,base,bg,ca,cs,da,de,el,en-gb,eo,es,eu,fi,fr,gl,he,hr,hu,id,is,it,ja,ko,librelogo,libreofficekit-data,lo,nb,nl,oc,pl,pt,pt-br,python-script-provider,pyuno,ru,sk,sl,sq,sv,tr,uk,vi,xsltfilter,zh-{cn,tw}} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Generate ssl keys, those are NOT used but required for the admin panel to startup
RUN touch ~/.rnd && \
    openssl genrsa -out "/etc/coolwsd/root.key.pem" 2048 && \
    openssl req -x509 -new -nodes -key "/etc/coolwsd/root.key.pem" -days 9131 -out "/etc/coolwsd/root.crt.pem" -subj "/C=DE/ST=BW/L=Berlin/O=Dummy Authority/CN=Dummy Authority" && \
    openssl genrsa -out "/etc/coolwsd/key.pem" 2048 && \
    openssl req -key "/etc/coolwsd/key.pem" -new -sha256 -out "/etc/coolwsd/localhost.csr.pem" -subj "/C=DE/ST=BW/L=Berlin/O=Dummy Authority/CN=localhost" && \
    openssl x509 -req -in "/etc/coolwsd/localhost.csr.pem" -CA "/etc/coolwsd/root.crt.pem" -CAkey "/etc/coolwsd/root.key.pem" -CAcreateserial -out "/etc/coolwsd/cert.pem" -days 9131 && \
    mv /etc/coolwsd/root.crt.pem /etc/coolwsd/ca-chain.cert.pem

RUN ln -s /app/data/fonts /opt/collaboraoffice/share/fonts/truetype/local

RUN cp /etc/coolwsd/coolwsd.xml /etc/coolwsd/coolwsd.xml.save
RUN rm -rf /etc/coolwsd/coolwsd.xml && ln -s /app/data/coolwsd.xml /etc/coolwsd/coolwsd.xml

RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

COPY package.json package-lock.json /app/code/
RUN npm install --production

COPY start.sh nginx.conf coolwsd.xml server.js /app/code/

COPY frontend /tmp/frontend
RUN cd /tmp/frontend/ && \
    npm i && \
    npm run build && \
    mv /tmp/frontend/dist /app/code/frontend && \
    rm -rf /tmp/frontend

# generate the en_US.UTF-8 locale for collabora to open files with special chars
RUN sed -e 's,^# en_US.UTF-8 UTF-8,en_US.UTF-8 UTF-8,' -i /etc/locale.gen && \
    locale-gen

# This will remove the selection touch handles
ADD branding.css /app/code/
RUN cat /app/code/branding.css >> /usr/share/coolwsd/browser/dist/branding.css

RUN setcap cap_fowner,cap_chown,cap_sys_chroot=ep /usr/bin/coolforkit-caps
RUN setcap cap_sys_admin=ep /usr/bin/coolmount

CMD [ "/app/code/start.sh" ]
