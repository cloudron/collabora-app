#!/usr/bin/env node

'use strict';

var exec = require('child_process').exec,
    express = require('express'),
    fs = require('fs'),
    morgan = require('morgan'),
    path = require('path'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    lastMile = require('connect-lastmile'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    oidc = require('express-openid-connect'),
    cheerio = require('cheerio');

const PORT = 3000;
const APP_ORIGIN = process.env.CLOUDRON_APP_ORIGIN || `http://localhost:${PORT}`;
const COOL_CONFIG_FILE = process.env.CLOUDRON ? '/app/data/coolwsd.xml' : path.resolve(__dirname, 'coolwsd_test.xml');

function isAuthenticated(req, res, next) {
    if (req.oidc.isAuthenticated()) return next();
    res.status(401).send({});
}

function oidcLogin(req, res) {
    res.oidc.login({
        returnTo: req.query.returnTo || '/',
        authorizationParams: {
            redirect_uri: `${APP_ORIGIN}/api/login-callback`,
        },
    });
}

function login(req, res, next) {
    if (!req.body.username || !req.body.password) {
        req.session.user = null;
        return next(new HttpError(401, 'Unauthorized'));
    }

    req.session.user = {
        id: req.body.username,
        username: req.body.username,
        displayName: req.body.username,
        email: 'mail@example.com'
    };

    next(new HttpSuccess(200, req.session.user));
}

function logout(req, res) {
    req.session.user = null
    res.redirect('/')
}

function getProfile(req, res, next) {
    next(new HttpSuccess(200, { user: req.session.user }))
}

async function healthcheck(req, res, next) {
    try {
        await fetch('http://localhost:9980/hosting/discovery');
    } catch (e) {
        return next(new HttpError(412, 'backend not yet ready'));
    }

    next(new HttpSuccess(200, {}));
}

function getSettings(req, res, next) {
    fs.readFile(COOL_CONFIG_FILE, function (error, result) {
        if (error) return next(new HttpError(500, error));

        const $ = cheerio.load(result, null, false);

        next(new HttpSuccess(200, {
            allowedHost: $('config storage wopi host').text(),
            adminConsole: {
                username: $('config admin_console username').text(),
                password: $('config admin_console password').text()
            }
        }));
    });
}

function setSettings(req, res, next) {
    fs.readFile(COOL_CONFIG_FILE, function (error, result) {
        if (error) return next(new HttpError(500, error));

        const $ = cheerio.load(result, null, false);

        // ensure we have all the DOM nodes we need
        if ($('config storage').length === 0) $('config').append('<storage></storage>');
        if ($('config storage wopi').length === 0) $('config storage').append('<wopi desc="Allow/deny wopi storage. Mutually exclusive with webdav." allow="true></wopi>');
        if ($('config storage wopi host').length === 0) $('config storage wopi').append('<host desc="Regex pattern of hostname to allow or deny." allow="true></host>');

        $('config storage wopi host').text(req.body.allowedHost.trim());

        fs.writeFile(COOL_CONFIG_FILE, $.xml(), function (error) {
            if (error) return next(new HttpError(500, error));

            if (process.env.CLOUDRON) {
                console.log('Restarting cool process');
                exec('supervisorctl restart cool');
            }

            next(new HttpSuccess(201, {}));
        });
    });
}

// Setup the express server and routes
const app = express();
const router = new express.Router();

router.get   ('/api/login', oidcLogin);
// router.post  ('/api/login', login);
// router.post  ('/api/logout', logout);
router.get   ('/api/settings', isAuthenticated, getSettings);
router.post  ('/api/settings', isAuthenticated, setSettings);
router.get   ('/api/profile', isAuthenticated, getProfile);
router.get   ('/api/healthcheck', healthcheck);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false, limit: '100mb' }));
app.use(session({ secret: 'office collab', resave: false, saveUninitialized: false }));

if (process.env.CLOUDRON_OIDC_ISSUER) {
    const SESSION_SECRET_FILE_PATH = '/app/data/.secret';
    if (!fs.existsSync(SESSION_SECRET_FILE_PATH)) {
        console.log('Generating new session secret...');
        fs.writeFileSync(SESSION_SECRET_FILE_PATH, require('crypto').randomBytes(20).toString('hex'), 'utf8');
    }

    // CLOUDRON_OIDC_PROVIDER_NAME is not supported
    app.use(oidc.auth({
        issuerBaseURL: process.env.CLOUDRON_OIDC_ISSUER,
        baseURL: APP_ORIGIN,
        clientID: process.env.CLOUDRON_OIDC_CLIENT_ID,
        clientSecret: process.env.CLOUDRON_OIDC_CLIENT_SECRET,
        secret: 'oidc-' + fs.readFileSync(SESSION_SECRET_FILE_PATH, 'utf8'),
        authorizationParams: {
            response_type: 'code',
            scope: 'openid profile email'
        },
        authRequired: false,
        routes: {
            callback: '/api/login-callback',
            login: false,
            logout: '/api/logout'
        },
        session: {
            name: 'CloudronCollaboraSession',
            rolling: true,
            rollingDuration: 24 * 60 * 60 * 4 // max 4 days idling
        },
    }));
} else {
    // mock oidc
    app.use((req, res, next) => {
        res.oidc = {
            login(options) {
                res.writeHead(200, { 'Content-Type': 'text/html' })
                res.write(require('fs').readFileSync(__dirname + '/oidc_develop_user_select.html', 'utf8').replaceAll('REDIRECT_URI', options.authorizationParams.redirect_uri));
                res.end()
            }
        };

        req.oidc = {
            user: {},
            isAuthenticated() {
                return !!req.session.username;
            }
        };

        if (req.session.username) {
            req.oidc.user = {
                sub: req.session.username,
                family_name: 'Cloudron',
                given_name: req.session.username.toUpperCase(),
                locale: 'en-US',
                name: req.session.username.toUpperCase() + ' Cloudron',
                preferred_username: req.session.username,
                email: req.session.username + '@cloudron.local',
                email_verified: true
            };
        }

        next();
    });

    app.use('/api/login-callback', (req, res) => {
        req.session.username = req.query.username;
        res.redirect(APP_ORIGIN);
    });

    app.use('/api/logout', (req, res) => {
        req.session.username = null;
        res.status(200).send({});
    });
}

app.use(router);
app.use('/', express.static(process.env.CLOUDRON ? './frontend/' : './frontend/dist/'));
app.use(lastMile());

const server = app.listen(PORT, function () {
    console.log(`Collabora admin listening on ${APP_ORIGIN}`);
});
