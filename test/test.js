#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    fs = require('fs'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const CUBBY_LOCATION = LOCATION + '-companion';
    const TEST_TIMEOUT = 20000;
    const EXEC_ARGS = { stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const DEFAULT_PATTERN = '[a-zA-Z0-9_\\-.]*';
    let pattern = DEFAULT_PATTERN;

    let browser, collaboraApp, cubbyApp, CUBBY_DOMAIN;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !collaboraApp || !cubbyApp) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(collaboraApp.domain) && !currentUrl.includes(cubbyApp.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login() {
        await browser.get(`https://${collaboraApp.fqdn}`);
        await waitForElement(By.id('loginButton'));
        await browser.findElement(By.id('loginButton')).click();

        await waitForElement(By.xpath('//h1[contains(text(), "Settings")]'));
    }

    async function checkSettings() {
        await browser.get(`https://${collaboraApp.fqdn}`);
        await waitForElement(By.xpath('//h1[contains(text(), "Settings")]'));
        const value = await browser.findElement(By.xpath('//form//input')).getAttribute('value');
        expect(value).to.equal(pattern);
    }

    async function saveSettings() {
        pattern = DEFAULT_PATTERN + 'example.com';

        await browser.get(`https://${collaboraApp.fqdn}`);
        await waitForElement(By.xpath('//h1[contains(text(), "Settings")]'));
        await browser.findElement(By.xpath('//form//input')).clear();
        await browser.findElement(By.xpath('//form//input')).sendKeys(pattern);
        await browser.findElement(By.xpath('//a[.="Save"]')).click();
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get(`https://${collaboraApp.fqdn}`);
        await waitForElement(By.xpath('//a[.="Logout"]'));
        await browser.findElement(By.xpath('//a[.="Logout"]')).click();
        await waitForElement(By.id('loginButton'));
    }

    function getAppInfo(appLocation) {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        const app = inspect.apps.filter(function (a) { return a.location === appLocation || a.location === appLocation + '2'; })[0];
        expect(app).to.be.an('object');
        return app;
    }

    function getCubbyAppInfo() {
        cubbyApp = getAppInfo(CUBBY_LOCATION);
        CUBBY_DOMAIN = cubbyApp.fqdn;
        return cubbyApp;
    }

    function getCollaboraAppInfo() {
        collaboraApp = getAppInfo(LOCATION);
        pattern = DEFAULT_PATTERN + collaboraApp.domain;
        return collaboraApp;
    }

    async function cubbyLogin() {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${CUBBY_DOMAIN}`);

        await waitForElement(By.id('loginButton'));
        await browser.findElement(By.id('loginButton')).click();

        await waitForElement(By.id('inputUsername'));
        await browser.findElement(By.id('inputUsername')).sendKeys(username);
        await browser.findElement(By.id('inputPassword')).sendKeys(password);
        await browser.findElement(By.id('loginSubmitButton')).click();

        await waitForElement(By.id('profileMenuDropdown'));
    }

    async function cubbyEnableCollaboraApp() {
        await browser.get(`https://${CUBBY_DOMAIN}/#settings`);

        await waitForElement(By.id('wopiHostnameInput'));
        await browser.findElement(By.id('wopiHostnameInput')).clear();
        await browser.findElement(By.id('wopiHostnameInput')).sendKeys(`https://${collaboraApp.fqdn}`);
        await browser.findElement(By.id('wopiHostnameInput')).sendKeys(Key.ENTER);

        await browser.sleep(5000);
    }

    function cubbyUploadTestFile() {
        execSync(`cloudron push --app ${CUBBY_DOMAIN} test.odt /app/data/data/${username}/test.odt`, EXEC_ARGS);
    }

    async function cubbyOpenEditor() {
        await browser.get('about:blank');

        await browser.get(`https://${CUBBY_DOMAIN}/#files/home/test.odt`);

        await browser.wait(async () => (await browser.getAllWindowHandles()).length === 2, 10000);

        //Loop through until we find a new window handle
        const tabs = await browser.getAllWindowHandles();
        for (const tab of tabs) {
            console.log(tab);
        }

        await browser.switchTo().window(tabs[1]);

        // wait when the document gets loaded
        await browser.sleep(5000);

        await waitForElement(By.xpath('//iframe[@name="document-viewer"]'));
        const appFrame = await browser.findElement(By.xpath('//iframe[@name="document-viewer"]'));

        // switch to iframe
        await browser.switchTo().frame(appFrame);

        // dismiss popup
        await waitForElement(By.xpath('//div[@class="iframe-welcome-wrap"]'));
        browser.executeScript("document.getElementsByClassName('iframe-welcome-wrap')[0].style.display = 'none';");
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//*[@id="File-tab-label"]')).click();
        await browser.sleep(2000);

        // switch to default content
        await browser.close();
        await browser.switchTo().window(tabs[0]);
    }


    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('install Cubby', async function () { execSync(`cloudron install --appstore-id io.cloudron.cubby --location ${CUBBY_LOCATION}`, EXEC_ARGS); });

    it('can get collabora app information', getCollaboraAppInfo);
    it('can get Cubby app information', async function () { getCubbyAppInfo(); });

    it('can login to Cubby', cubbyLogin);
    it('can enable Collabora app in Cubby', cubbyEnableCollaboraApp);
    it('can upload file to Cubby', cubbyUploadTestFile);

    it('can login', login);
    it('can see settings', checkSettings);
    it('can logout', logout);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('restart app', function () { execSync(`cloudron restart --app ${collaboraApp.id}`, EXEC_ARGS); });

    it('can login', login);
    it('can see settings', checkSettings);
    it('can login', logout);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('backup app', function () { execSync(`cloudron backup create --app ${collaboraApp.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${collaboraApp.id}`));
        execSync(`cloudron uninstall --app ${collaboraApp.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getCollaboraAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${collaboraApp.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can see settings', checkSettings);

    it('can open editor in Cubby', cubbyOpenEditor);

    it('move to different location', function () { execSync(`cloudron configure --location ${LOCATION}2 --app ${collaboraApp.id}`, EXEC_ARGS); });

    it('can get collabora app information', getCollaboraAppInfo);
    it('can login', login);
    it('can see settings', checkSettings);

    it('can enable Collabora app in Cubby', cubbyEnableCollaboraApp);
    it('can open editor in Cubby', cubbyOpenEditor);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${collaboraApp.id}`, EXEC_ARGS); });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id com.collaboraoffice.coudronapp --location ${LOCATION}`, EXEC_ARGS);
        pattern = DEFAULT_PATTERN;
    });

    it('can get collabora app information', getCollaboraAppInfo);

    it('can login', login);
    it('can see settings', checkSettings);
    it('can enable Collabora app in Cubby', cubbyEnableCollaboraApp);
    it('can open editor in Cubby', cubbyOpenEditor);

    it('can update', function () { execSync(`cloudron update --app ${collaboraApp.id}`, EXEC_ARGS); });
    it('can get collabora app information', getCollaboraAppInfo);

    it('can get settings', checkSettings);
    it('can open editor in Cubby', cubbyOpenEditor);

    it('uninstall app', function () { execSync(`cloudron uninstall --app ${collaboraApp.id}`, EXEC_ARGS); });
    it('uninstall cubby', function () { execSync(`cloudron uninstall --app ${cubbyApp.id}`, EXEC_ARGS); });
});
